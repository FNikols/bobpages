
[![pipeline status](https://gitlab.com/FNikols/bobpages/badges/main/pipeline.svg)](https://gitlab.com/FNikols/bobpages/-/commits/main)

[![coverage report](https://gitlab.com/FNikols/bobpages/badges/main/coverage.svg)](https://gitlab.com/FNikols/bobpages/-/commits/main)

[![Latest Release](https://gitlab.com/FNikols/bobpages/-/badges/release.svg)](https://gitlab.com/FNikols/bobpages/-/releases)
